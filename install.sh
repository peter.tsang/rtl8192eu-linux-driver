# Clone repository and change directory to cloned path.
git clone https://gitlab.com/peter.tsang/rtl8192eu-linux-driver.git;
cd rtl8192eu-linux-driver;

# Building and installing using DKMS.
#  Install DKMS for raspberry pi.
sudo apt-get install git raspberrypi-kernel-headers build-essential dkms;

# Add the driver to DKMS
sudo dkms add .;

Build and install the driver.
sudo dkms install rtl8192eu/1.0;

# Distributions based on Debian & Ubuntu have RTL8XXXU driver present & running in kernelspace. To use our RTL8192EU driver, we need to blacklist RTL8XXXU.
echo "blacklist rtl8xxxu" | sudo tee /etc/modprobe.d/rtl8xxxu.conf;

# Force RTL8192EU Driver to be active from boot.
echo -e "8192eu\n\nloop" | sudo tee /etc/modules;

# Newer versions of Ubuntu has weird plugging/replugging issue (Check #94). This includes weird idling issues, To fix this:
echo "options 8192eu rtw_power_mgnt=0 rtw_enusbss=0" | sudo tee /etc/modprobe.d/8192eu.conf;


# Update changes to Grub & initramfs
sudo update-grub; sudo update-initramfs -u;

# Reboot system to load new changes from newly generated initramfs.
systemctl reboot -i;

